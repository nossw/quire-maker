==========
Quiremaker
==========

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Run `python manage.py` to create start the server.

2. Visit http://127.0.0.1:8000/quiremaker/ to access the app.