from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.template import Context, loader
from django.views.decorators.csrf import csrf_exempt
import pdfkit
from PyPDF2 import PdfFileReader, PdfFileWriter
from PyPDF2.pdf import PageObject
import os

garbageCollect = []


@csrf_exempt
def index(request):
    template = loader.get_template("quiremaker/index.html")
    return HttpResponse(template.render())


@csrf_exempt
def makePage(x, y, order, h, w):
    # Switch height and width when making blank page for landscape mode rather than portrait.
    newPage = PageObject.createBlankPage(pdf=None, width=h * 4, height=w * 2)
    for i in range(x, y):
        path = "quiremaker/pages/page" + str(order[i]-1).zfill(2) + ".pdf"
        f = open(path, 'rb')
        garbageCollect.append(f)
        reader = PdfFileReader(f)
        page = reader.getPage(0)
        # Create top of page
        if i < x + 4:
            newPage.mergeTranslatedPage(page, h * (i % 4), w, expand=False)
        # Bottom of page
        else:
            newPage.mergeRotatedTranslatedPage(page, 180, h * (i % 4)/2 + h/2, w/2, expand=False)
    return newPage


@csrf_exempt
def submitted(request):
    form = request.POST
    content = form["content"]
    pageWidth = (float(form["page_width"])/2)*25.4 #Convert to mm from in
    pageHeight = (float(form["page_height"])/4)*25.4
    fontSize = form["font_size"]
    numPages = int(form["pages"])
    includePageNums = form.get("include_page_numbers", False) #boolean
    print("WHAT IS HAPPENING")
    pages16 = [2,15,14,3,7,10,11,6,8,9,12,5,1,16,13,4]
    pages32 = [2,31,30,3,7,26,27,6,8,25,28,5,1,32,29,4,10,23,22,11,15,18,19,14,16,17,20,13,9,24,21,12]

    #Create css file
    with open("quiremaker/quiremaker.css", "w") as f:
        f.write("p { font-size: " + fontSize + ";\n}\n* { font-family: Arial, sans-serif;\n}\n")
        f.close()

    #Create an html file out of the content, overwrite previous contents.
    with open("quiremaker/content.html", "w") as f:
        f.write("<link rel='stylesheet' type='text/css' href='quiremaker.css'>\n" + content)
        f.close()

    #Set options for pdf creation from the html file
    options = {"page-width": pageHeight, "page-height": pageWidth, "user-style-sheet": "quiremaker.css",}

    if includePageNums == "true":
        options["header-center"] = "([page])"

    path = r'C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe'
    config = pdfkit.configuration(wkhtmltopdf=path)
    pdfkit.from_file("quiremaker/content.html", "quiremaker/converted.pdf", options=options, configuration=config)

    # Chop pdf into separate pages, add as many blank pages as needed to fulfill numPages
    inpf = open("quiremaker/converted.pdf", "rb")
    garbageCollect.append(inpf)
    inputpdf = PdfFileReader(inpf)

    pagesMade = []

    if not os.path.exists('quiremaker/pages'):
        os.makedirs('quiremaker/pages')

    for i in range(inputpdf.numPages):
        output = PdfFileWriter()
        pagesMade.append(i)
        output.addPage(inputpdf.getPage(i))
        with open("quiremaker/pages/page%s.pdf" % str(i).zfill(2), "wb") as outputStream:
            output.write(outputStream)
            garbageCollect.append(outputStream)

    for i in range(numPages):
        pageName = "quiremaker/pages/page" + str(i).zfill(2) + ".pdf"
        options["page-offset"] = i
        if i not in pagesMade:
            pdfkit.from_file("quiremaker/empty.html", pageName, options=options, configuration=config)

    # pdfkit uses mm as units, convert to usu for PyPDF2 (1 inch = 72 usu)
    conv = (1/25.4)*72
    pageWidth = conv*pageWidth
    pageHeight = conv*pageHeight

    # Make pages individually
    if numPages == 16:
        order = pages16
    else:
        order = pages32

    page1 = makePage(0, 8, order, pageHeight, pageWidth)
    page2 = makePage(8, 16, order, pageHeight, pageWidth)
    writer = PdfFileWriter()

    writer.addPage(page1)
    writer.addPage(page2)

    if numPages == 32:
        page3 = makePage(16, 24, order, pageHeight, pageWidth)
        page4 = makePage(24, 32, order, pageHeight, pageWidth)

        writer.addPage(page3)
        writer.addPage(page4)


    # Compile the pages into single file, avoid overwriting previous quire files.
    if not os.path.exists('quire.pdf'):
        with open('quire.pdf', 'wb') as f:
            writer.write(f)
            f.close()
    else:
        i = 1
        path = 'quire (' + str(i) + ').pdf'
        while os.path.exists(path):
            i += 1
            path = 'quire (' + str(i) + ').pdf'
        with open(path, 'wb') as f:
            writer.write(f)
            f.close()

    # Cleanup intermediate files.
    for item in garbageCollect:
        item.close()

    cleanup = ["converted.pdf", "pages", "content.html", "quiremaker.css"]
    for path in cleanup:
        path = os.path.join("quiremaker", path)
        if os.path.isfile(path):
            fullPath = path
            os.unlink(fullPath)
        elif os.path.isdir(path):
            for f in os.listdir(path):
                fullPath = os.path.join(path, f)
                os.unlink(fullPath)

    template = loader.get_template("quiremaker/submitted.html")
    return HttpResponse(template.render())