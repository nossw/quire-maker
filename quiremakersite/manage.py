#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys
import xml.etree.ElementTree as etree


def main():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'quiremakersite.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc


    port = None
    try:
        tree = etree.parse("WebXapp\\PortReader.xml").getroot()
        port = tree.find("Port").text.strip()
    except:
        port = "8000"

    # if os.system("start chrome http:\\localhost:8000/quiremaker") == 1:
    #     if os.system("start firefox http:\\localhost:8000/quiremaker") == 1:
    #         if os.system("start microsoft-edge:http:\\127.0.0.1:8000/quiremaker") == 1:
    #             os.system("start iexplore http:\\127.0.0.1:8000/quiremaker ")

    execute_from_command_line(["manage.py", "runserver", "0:%s"%port])



if __name__ == '__main__':
    main()
